<?php

use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arrData = [];
        for($i = 20; $i < 120; $i++) {
            $data = [
                'username' => 'ntdat'.$i,
                'password' => \Illuminate\Support\Facades\Hash::make('123456'),
                'status' => rand(0, 1),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ];
            array_push($arrData, $data);
        }
        \Illuminate\Support\Facades\DB::table('admins')->insert($arrData);
    }
}
