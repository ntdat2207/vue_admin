<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVietlotStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vietlot_statistics', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('number');
            $table->integer('statistics');
            $table->dateTime('last_date');
            $table->index('number');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vietlot_statistics');
    }
}
