<?php

namespace App\Http\Controllers;

use App\Helpers\HttpRequestHelper;
use App\Helpers\TelegramLogHelper;
use Illuminate\Http\Request;

class GetPetController extends Controller
{
    public function getPet(Request $request) {
        $petTypeIdArr = [
            '1001' => 'Lợn - Pifating',
            '1002' => 'Sâu - Woruffing',
            '1003' => 'Chuột - Radenting',
            '2001' => 'Rắn - Sniffing',
            '2003' => 'Ngựa - Horiding',
            '3001' => 'Dơi - Bawaving'
        ];
        $petTypeId = $request->get('pet_type_id');
        $url = 'https://api.gunstar.io/v1/markets/pets?limit=8&page=0&sort=price_asc&rarity=';
        if($petTypeId) {
            $url .= '&petTypeId='.$petTypeId;
        }
        $maxPrice = 250000000000000000000;
        $callApi = HttpRequestHelper::callApi([], $url, [], 'get');
        if($callApi->status != 'success') {
            return;
        }
        $pets = $callApi->data->docs;
        foreach ($pets as $pet) {
            $price = $pet->market->price;
            if($price <= $maxPrice) {
                $tokenId = $pet->tokenId;
                $link = 'https://dapp.gunstar.io/pets/' . $tokenId;
                $petType = $petTypeIdArr[$pet->petTypeID];
                TelegramLogHelper::logGunstar($link, $price, $petType);
            }
        }
    }

    public function getIp(Request $request) {
        dd($request->ip());
    }
}
