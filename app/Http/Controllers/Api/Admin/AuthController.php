<?php

namespace App\Http\Controllers\Api\Admin;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    /**
     * @OA\Post(
     *   path="/api/admin/login",
     *   summary="Sign in",
     *   description="Login by username, password",
     *   operationId="authLogin",
     *   tags={"auth"},
     *   @OA\RequestBody(
     *     required=true,
     *     description="Pass user credentials",
     *     @OA\JsonContent(
     *       required={"username","password"},
     *       @OA\Property(property="username", type="string", format="text", example="ntdat"),
     *       @OA\Property(property="password", type="string", format="password", example="123456"),
     *     ),
     *   ),
     *   @OA\Response(
     *     response=422,
     *     description="Unprocessable Entity",
     *     @OA\JsonContent(
     *       @OA\Property(property="code", type="integer", example="0"),
     *       @OA\Property(property="message", type="string", example="username is required"),
     *       @OA\Property(property="data", type="object", example="{}"),
     *     )
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="success",
     *     @OA\JsonContent(
     *       @OA\Property(property="code", type="integer", example="1"),
     *       @OA\Property(property="message", type="string", example="Login Success"),
     *       @OA\Property(property="data", type="object",
     *         @OA\Property(property="access_token", type="string", example="Token here"),
     *         @OA\Property(property="token_type", type="string", example="Bearer"),
     *         @OA\Property(property="expires_at", type="string", example="2021-12-12 00:00:00")
     *       ),
     *     )
     *   ),
     *   @OA\Response(
     *     response=401,
     *     description="Unauthenticated",
     *     @OA\JsonContent(
     *       @OA\Property(property="code", type="integer", example="0"),
     *       @OA\Property(property="message", type="string", example="Credentials are invalid"),
     *       @OA\Property(property="data", type="object", example="{}"),
     *     )
     *   ),
     * )
     */
    public function login(Request $request) {
        $requestData = json_decode($request->getContent(), true);
        $validate = Validator::make($requestData, [
            'username' => 'required',
            'password' => 'required'
        ], [
            'username.required' => 'Tên đăng nhập không được để trống',
            'password.required' => 'Mật khẩu không được để trống'
        ]);
        if($validate->fails()) {
            return ResponseHelper::returnResponse(0, $validate->errors()->first(), $validate->errors(), 422);
        }
        $credentials = [
            'username' => $requestData['username'],
            'password' => $requestData['password'],
            'status' => config('apps.admin.status.active')
        ];
        $login = Auth::attempt($credentials);
        if(!$login) {
            return ResponseHelper::returnResponse(0, 'Tài khoản hoặc mật khẩu không đúng', null, 401);
        }
        $user = Auth::user();
        $tokenResult = $user->createToken('Personal-Access-Token');
        $token = $tokenResult->token;
        $token->expires_at = Carbon::now()->addDays(1)->format('Y-m-d H:i:s');
        $token->save();


        $responseData = [
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse($tokenResult->token->expires_at)->format('Y-m-d H:i:s')
        ];
        return ResponseHelper::returnResponse(1, 'Đăng nhập thành công', $responseData, 200);
    }

    /**
     * @OA\Post(
     *   path="/api/admin/logout",
     *   summary="Logout",
     *   description="Logout",
     *   operationId="authLogout",
     *   tags={"auth"},
     *   security={{"bearerAuth":{}}},
     *   @OA\Response(
     *     response=200,
     *     description="success",
     *     @OA\JsonContent(
     *       @OA\Property(property="code", type="integer", example="1"),
     *       @OA\Property(property="message", type="string", example="Logout Success"),
     *       @OA\Property(property="data", type="object", example="{}"),
     *     )
     *   ),
     *   @OA\Response(
     *     response=401,
     *     description="Unauthenticated",
     *     @OA\JsonContent(
     *       @OA\Property(property="code", type="integer", example="0"),
     *       @OA\Property(property="message", type="string", example="Unauthorized"),
     *       @OA\Property(property="data", type="object", example="{}"),
     *     )
     *   ),
     * )
     */
    public function logout(Request $request) {
        Auth::guard('api')->user()->token()->revoke();
        return ResponseHelper::returnResponse(1, "Đăng xuất thành công", null, 200);
    }
}
