<?php

namespace App\Http\Controllers\Api\Admin;

use App\Helpers\ResponseHelper;
use App\Repositories\AdminRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class AdminController extends Controller
{
    private $adminRepository;

    public function __construct() {
        $this->adminRepository = new AdminRepository();
    }

    /**
     * @OA\Get(
     *   path="/api/admin/admins",
     *   summary="List admin",
     *   description="List admin",
     *   operationId="adminLists",
     *   tags={"admins"},
     *   security={{"bearerAuth":{}}},
     *   @OA\Parameter(name="username", in="query", description="Ten dang nhap cua admin"),
     *   @OA\Parameter(name="status", in="query", description="Trang thai: 1-hoat dong, 0-khong hoat dong"),
     *   @OA\Parameter(name="page", in="query", description="Page"),
     *   @OA\Response(
     *     response=200,
     *     description="success",
     *     @OA\JsonContent(
     *       @OA\Property(property="code", type="integer", example="1"),
     *       @OA\Property(property="message", type="string", example="Login Success"),
     *       @OA\Property(property="data", type="object",
     *         @OA\Property(property="current_page", type="integer", example="1"),
     *         @OA\Property(property="first_page_url", type="string", example="http://45.76.223.45:9099/api/admin/admins?page=1"),
     *         @OA\Property(property="from", type="integer", example="1"),
     *         @OA\Property(property="last_page", type="integer", example="11"),
     *         @OA\Property(property="last_page_url", type="string", example="http://45.76.223.45:9099/api/admin/admins?page=11"),
     *         @OA\Property(property="next_page_url", type="string", example="http://45.76.223.45:9099/api/admin/admins?page=2"),
     *         @OA\Property(property="path", type="string", example="http://45.76.223.45:9099/api/admin/admins"),
     *         @OA\Property(property="per_page", type="integer", example="10"),
     *         @OA\Property(property="prev_page_url", type="string", example=""),
     *         @OA\Property(property="to", type="integer", example="10"),
     *         @OA\Property(property="total", type="integer", example="101"),
     *         @OA\Property(property="data", type="array", description="Danh sach admin",
     *           @OA\Items(type="object",
     *             @OA\Property(property="username", type="string", description="Ten dang nhap"),
     *             @OA\Property(property="status", type="integer", description="Trang thai: 1-hoat dong, 0-khong hoat dong"),
     *             @OA\Property(property="created_at", type="string", description="Thoi gian, dinh dang: YYYY-MM-DD HH:II:SS"),
     *             @OA\Property(property="updated_at", type="string", description="Thoi gian, dinh dang: YYYY-MM-DD HH:II:SS"),
     *             @OA\Property(property="status_str", type="string", description="Trang thai dang chu"),
     *           ),
     *         ),
     *       ),
     *     )
     *   ),
     *   @OA\Response(
     *     response=401,
     *     description="Unauthenticated",
     *     @OA\JsonContent(
     *       @OA\Property(property="code", type="integer", example="0"),
     *       @OA\Property(property="message", type="string", example="Credentials are invalid"),
     *       @OA\Property(property="data", type="object", example="{}"),
     *     )
     *   ),
     * )
     */
    public function index(Request $request)
    {
        //
        $filters = $request->all();
        $admins = $this->adminRepository->getPaginate(['username', 'status', 'created_at', 'updated_at'], $filters, 10);
        if($admins) {
            $admins = $this->adminRepository->formatAllRecord($admins);
        }
        return ResponseHelper::returnResponse(ResponseHelper::$codeSuccess, 'Success', $admins, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @OA\Post(
     *   path="/api/admin/admins",
     *   summary="Store admin",
     *   description="Store admin",
     *   operationId="adminStore",
     *   tags={"admins"},
     *   security={{"bearerAuth":{}}},
     *   @OA\RequestBody(
     *     required=true,
     *     description="Data to create admin",
     *     @OA\JsonContent(
     *       required={"username","password","status"},
     *       @OA\Property(property="username", type="string", format="text", example="ntdat"),
     *       @OA\Property(property="password", type="string", format="password", example="123456"),
     *       @OA\Property(property="status", type="integer", format="number", example="1",description="1: active, 0: inactive"),
     *     ),
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="success",
     *     @OA\JsonContent(
     *       @OA\Property(property="code", type="integer", example="1"),
     *       @OA\Property(property="message", type="string", example="Create admin success"),
     *       @OA\Property(property="data", type="object",
     *
     *       ),
     *     )
     *   ),
     *   @OA\Response(
     *     response=401,
     *     description="Unauthenticated",
     *     @OA\JsonContent(
     *       @OA\Property(property="code", type="integer", example="0"),
     *       @OA\Property(property="message", type="string", example="Credentials are invalid"),
     *       @OA\Property(property="data", type="object", example="{}"),
     *     )
     *   ),
     *   @OA\Response(
     *     response=422,
     *     description="Validate fail",
     *     @OA\JsonContent(
     *       @OA\Property(property="code", type="integer", example="0"),
     *       @OA\Property(property="message", type="string", example="Validate fail"),
     *       @OA\Property(property="data", type="object",
     *
     *       ),
     *     )
     *   ),
     * )
     */
    public function store(Request $request)
    {
        //
        $requestData = json_decode($request->getContent(), 1);
        $validate = Validator::make($requestData, [
            'username' => 'required|unique:admins',
            'password' => 'required|min:6',
            'status' => Rule::in(config('apps.admin.status'))
        ], [
            'username.required' => 'Tên đăng nhập không được để trống',
            'username.unique' => 'Tên đăng nhập đã tồn tại',
            'password.required' => 'Mật khẩu không được để trống',
            'password.min' => 'Mật khẩu phải có ít nhất 6 kí tự',
            'status.in' => 'Trạng thái không hợp lệ'
        ]);
        if($validate->fails()) {
            return ResponseHelper::returnResponse(ResponseHelper::$codeFail, 'Validate fail', $validate->errors(), 422);
        }
        $saveAdmin = $this->adminRepository->store($requestData);
        if(!$saveAdmin) {
            return ResponseHelper::returnResponse(ResponseHelper::$codeFail, 'Lưu thông tin thất bại', [], 400);
        }
        return ResponseHelper::returnResponse(ResponseHelper::$codeSuccess, 'Lưu thông tin thành công', [], 200);
    }

    /**
     * @OA\Get(
     *   path="/api/admin/admins/{id}",
     *   summary="Show 1 admin",
     *   description="Show 1 admin",
     *   operationId="adminShow",
     *   tags={"admins"},
     *   security={{"bearerAuth":{}}},
     *   @OA\Parameter(name="id", in="path", description="ID admin", required=true),
     *   @OA\Response(
     *     response=200,
     *     description="success",
     *     @OA\JsonContent(
     *       @OA\Property(property="code", type="integer", example="1"),
     *       @OA\Property(property="message", type="string", example="Success"),
     *       @OA\Property(property="data", type="object",
     *         @OA\Property(property="user_name", type="string", example="ntdat"),
     *         @OA\Property(property="id", type="integer", example="1"),
     *         @OA\Property(property="status", type="integer", example="1"),
     *         @OA\Property(property="status_str", type="string", example="Active")
     *       ),
     *     )
     *   ),
     *   @OA\Response(
     *     response=401,
     *     description="Unauthenticated",
     *     @OA\JsonContent(
     *       @OA\Property(property="code", type="integer", example="0"),
     *       @OA\Property(property="message", type="string", example="Credentials are invalid"),
     *       @OA\Property(property="data", type="object", example="{}"),
     *     )
     *   ),
     * )
     */
    public function show($id)
    {
        //
        if(empty($id) || !$id) {
            return ResponseHelper::returnResponse(ResponseHelper::$codeFail, 'Please enter id', [], 422);
        }
        $select = ['id', 'status', 'username', 'created_at', 'updated_at'];
        $filters = ['username' => $id];
        $admin = $this->adminRepository->getOne($select, $filters);
        if($admin) {
            $admin = $this->adminRepository->formatRecord($admin);
            $code = ResponseHelper::$codeSuccess;
            $message = 'Success';
            $httpStatusCode = 200;
        } else {
            $code = ResponseHelper::$codeFail;
            $message = 'User does not exist';
            $httpStatusCode = 400;
        }
        return ResponseHelper::returnResponse($code, $message, $admin, $httpStatusCode);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
