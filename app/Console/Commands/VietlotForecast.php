<?php

namespace App\Console\Commands;

use App\Models\VietlotStatistics;
use Illuminate\Console\Command;

class VietlotForecast extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'forecast:vietlot';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Forecast vietlot';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $numbers = VietlotStatistics::orderBy('statistics', 'ASC')->orderBy('last_date', 'ASC')->get()->pluck('number');
        dd($numbers);
    }
}
