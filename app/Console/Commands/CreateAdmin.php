<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Admin;
use Illuminate\Support\Facades\Hash;

class CreateAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:admin {username} {password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Tao tai khoan admin moi';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $username = $this->argument('username');
        $password = $this->argument('password');

        if (Admin::where('username', $username)->count()) {
            return $this->error('=> Email already exists.');
        }

        $admin = new Admin();
        $admin->username = $username;
        $admin->password = Hash::make($password);
        $admin->status = config('apps.admin.status.active');
        if ($admin->save()) {
            $this->info("=> Created account $username / $password");
        } else {
            $this->info("=> Error.");
        }
    }
}
