<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Schema;
use Storage;

class CreateRepository extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:repo {repoName} {model} {table}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new repository';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // //
        $repoName = $this->argument('repoName');
        $model = $this->argument('model');
        $table = $this->argument('table');

        $path = app_path("Repositories/".$repoName.".php");

        $columns = Schema::getColumnListing($table);
        $columnsStr = "[";
        foreach($columns as $colName) {
            $columnsStr .= "'".$colName."', ";
        }
        $columnsStr .= "]";
        $content = $this->fileContent($repoName, $model, $columnsStr);
        $fileName = $repoName.".php";
        Storage::disk('repo')->put($fileName, $content);

        echo "Repository created successfully." . PHP_EOL;
    }

    protected function fileContent($repoName, $model, $columns) {
        $content = file_get_contents(app_path('Repositories/BaseRepository.php'));
        $content = str_replace("BaseRepository", $repoName, $content);
        $content = str_replace("BaseModel", $model, $content);
        $content = str_replace('"Hello"', $columns, $content);
        return $content;
    }

}
