<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CrawVietlot extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'craw:vietlot';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Craw data from vietlot';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $html = file_get_html('https://atrungroi.com/xo-so-power-6-55-vietlott/thong-ke-tan-suat-xo-so-power-6-55.html?sap-xep=thu-tu&tu-ngay=2019-05-01&den-ngay=2022-05-19');
        $table = $html->find('.table-responsive', 0);
        $tbody = $table->find('tbody', 0);
        $trs = $tbody->getElementsByTagName('tr');
        $numbers = [];
        foreach($trs as $key => $tr) {
            $str = $tr->find('.td_solanra', 0)->plaintext;
            $position = 0;
            for($i = 0; $i <= strlen($str) - 1; $i++) {
                if($str[$i] == ' ') {
                    $position = $i;
                    break;
                }
            }
            $date = $tr->find('.td_ngayra', 0)->find('span', 0)->plaintext;
            $arrDate = explode("/", $date);
            $newDate = $arrDate[2] . '-' . str_pad($arrDate[1], 2, 0, STR_PAD_LEFT) . '-' . str_pad($arrDate[0], 2, 0, STR_PAD_LEFT);
            $numbers[] = [
                'number' => $tr->find('.td_boso', 0)->find('span', 0)->plaintext,
                'statistics' => substr($str, 0, $position),
                'last_date' => $newDate,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ];
        }
        DB::table('vietlot_statistics')->truncate();
        DB::table('vietlot_statistics')->insert($numbers);
        $this->info('DONE');
    }
}
