<?php

namespace App\Console\Commands;

use App\Helpers\HttpRequestHelper;
use App\Helpers\TelegramLogHelper;
use Illuminate\Console\Command;

class GetWrongPricePetGst extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:pet {pet_type_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get pet then send link to telegram';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $petTypeId = $this->argument('pet_type_id');
        $url = 'https://api.gunstar.io/v1/markets/pets?limit=8&page=0&sort=price_asc&rarity=';
        if($petTypeId) {
            $url .= '&petTypeId='.$petTypeId;
        }
        $maxPrice = 200000000000000000000;
        $callApi = HttpRequestHelper::callApi([], $url, [], 'get');
        if($callApi->status != 'success') {
            return;
        }
        $pets = $callApi->data->docs;
        foreach ($pets as $pet) {
            $price = $pet->market->price;
            if($price <= $maxPrice) {
                $tokenId = $pet->tokenId;
                $link = 'https://dapp.gunstar.io/pets/' . $tokenId;
                TelegramLogHelper::logGunstar($link);
            }
        }
    }
}
