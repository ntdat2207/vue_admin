<?php

namespace App\Repositories;

class AdminRepository extends Repository {

	public function __construct() {
		parent::__construct(new \App\Models\Admin());
		$this->fields = ['created_at', 'deleted_at', 'id', 'password', 'updated_at', 'username', 'status'];
	}

	public function formatRecord($record) {
	    $record->status_str = config('apps.admin.status_str')[$record->status];
		return $record;
	}

	public function formatAllRecord($records) {
		foreach($records as $record) {
			$this->formatRecord($record);
		}
		return $records;
	}

}
?>
