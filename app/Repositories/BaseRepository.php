<?php

namespace App\Repositories;

class BaseRepository extends Repository {
	
	public function __construct() {
		parent::__construct(new \App\Models\BaseModel());
		$this->fields = "Hello";
	}

	public function formatRecord($record) {
		return $record;
	}

	public function formatAllRecord($records) {
		foreach($records as $record) {
			$this->formatRecord($record);
		}		
		return $records;
	}

} 
?>
