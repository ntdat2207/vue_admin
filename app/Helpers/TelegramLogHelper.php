<?php


namespace App\Helpers;


class TelegramLogHelper
{
    /**
    *
    * 2021-05-25
    *
    * @param object
    *
    * @author ntdat <datnt@baokim.vn>
    * @return
    */
    public static function logGunstar($url, $price, $petType) {
        if(env('TELEGRAM_LOG', false)) {
            $data = [
                'chat_id' => env('TELEGRAM_LOGGER_CHAT_ID'),
                'parse_mode' => 'html'
            ];

            $text = "<strong>GUNSTAR CHEAP PRICE</strong>%0A";
            $text .= "Price: $price%0A";
            $text .= "Type: $petType%0A";
            $text .= "Purchase at: $url%0A";

            $data['text'] = $text;
            $httpQuery = urldecode(http_build_query($data));
            $url = 'https://api.telegram.org/bot' . env('TELEGRAM_LOGGER_BOT_TOKEN') . '/sendMessage?' . $httpQuery;
            file_get_contents($url);
        }
    }
}
