<?php


namespace App\Helpers;


class ResponseHelper
{
    public static $codeSuccess = 1;
    public static $codeFail = 0;
    /**
    *
    * 2021-10-13
    *
    * @param int $code
    * @param string $message
    * @param array $data
    *
    * @author ntdat <datnt@baokim.vn>
    * @return array
    */
    public static function formatResponse($code, $message, $data) {
        if(is_array($data)) {
            $data = (object) $data;
        }
        return [
            'code' => $code,
            'message' => $message,
            'data' => $data
        ];
    }

    /**
    *
    * 2021-10-13
    *
    * @param int $code
    * @param string $message
    * @param object|array $data
    * @param int $httpStatusCode
    *
    * @author ntdat <datnt@baokim.vn>
    * @return string
    */
    public static function returnResponse($code, $message, $data, $httpStatusCode) {
        $formatted = self::formatResponse($code, $message, $data);
        return response()->json($formatted, $httpStatusCode);
    }
}
