<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('get-pet', 'GetPetController@getPet');
Route::group(['prefix' => 'admin'], function() {
    Route::post('login', 'Api\Admin\AuthController@login')->name('admin.login');
    Route::group(['middleware' => 'auth.check_api_token'], function() {
        Route::post('logout', 'Api\Admin\AuthController@logout')->name('admin.logout');

        Route::resource('admins', 'Api\Admin\AdminController');
    });
});
