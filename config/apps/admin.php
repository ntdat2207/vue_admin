<?php
return [
    'status' => [
        'active' => 1,
        'inactive' => 0
    ],
    'status_str' => [
        1 => 'Đang hoạt động',
        0 => 'Không hoạt động'
    ]
];
